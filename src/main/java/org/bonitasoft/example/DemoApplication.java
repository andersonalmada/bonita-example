/**
 * Copyright (C) 2019 Bonitasoft S.A.
 * Bonitasoft, 32 rue Gustave Eiffel - 38000 Grenoble
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation
 * version 2.1 of the License.
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301, USA.
 **/
package org.bonitasoft.example;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bonitasoft.engine.api.APIClient;
import org.bonitasoft.engine.bpm.bar.BusinessArchive;
import org.bonitasoft.engine.bpm.bar.BusinessArchiveBuilder;
import org.bonitasoft.engine.bpm.bar.actorMapping.Actor;
import org.bonitasoft.engine.bpm.bar.actorMapping.ActorMapping;
import org.bonitasoft.engine.bpm.contract.Type;
import org.bonitasoft.engine.bpm.flownode.GatewayType;
import org.bonitasoft.engine.bpm.flownode.HumanTaskInstance;
import org.bonitasoft.engine.bpm.process.ProcessDefinition;
import org.bonitasoft.engine.bpm.process.ProcessDeployException;
import org.bonitasoft.engine.bpm.process.ProcessInstance;
import org.bonitasoft.engine.bpm.process.impl.ProcessDefinitionBuilder;
import org.bonitasoft.engine.expression.Expression;
import org.bonitasoft.engine.expression.ExpressionBuilder;
import org.bonitasoft.engine.expression.InvalidExpressionException;
import org.bonitasoft.engine.identity.User;
import org.bonitasoft.engine.operation.Operation;
import org.bonitasoft.engine.operation.OperationBuilder;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class DemoApplication {

	static APIClient apiClient = new APIClient();

	public static String TENANT_ADMIN_NAME = "install";
	public static String TENANT_ADMIN_PASSWORD = "install";

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);

		int decisions = 611;
		int vars = 5;

		loginAsTenantAdministrator();
		User requester = getUserByUserName("requester");
		loginWithAnotherUser(requester);

		Actor requester1 = new Actor("Requester");
		requester1.addUser(requester.getUserName());
		ActorMapping actorMapping = new ActorMapping();
		actorMapping.addActor(requester1);

		ProcessDefinitionBuilder processBuilder = new ProcessDefinitionBuilder().createNewInstance("bonita" + decisions,
				"bonita" + decisions);

		processBuilder.addActor(requester1.getName(), true);
		processBuilder.addStartEvent("start");
		processBuilder.addUserTask("UserTask0", requester1.getName()).addContract().addInput("var0", Type.DECIMAL,
				"var0").addData("var0", "java.lang.Double", null);
		processBuilder.addUserTask("UserTask1", requester1.getName()).addContract().addInput("var1",
				Type.DECIMAL, "var1").addData("var1", "java.lang.Double", null);
		
		for (int v = 2; v < vars; v++) {
			processBuilder.addUserTask("UserTask" + v, requester1.getName()).addContract().addInput("var" + v,
					Type.DECIMAL, "var" + v).addData("var" + v, "java.lang.Double", null);
			;
			processBuilder.addTransition("UserTask" + (v - 1), "UserTask" + v);
		}

		processBuilder.addEndEvent("end");

		processBuilder.addTransition("start", "UserTask0");
		processBuilder.addTransition("UserTask" + (vars - 1), "end");

		processBuilder.addGateway("gateway", GatewayType.EXCLUSIVE);
		processBuilder.addTransition("UserTask0", "gateway");

		try {
            processBuilder.addTransition("gateway", "UserTask1",
                    // let's simulate a human decision with a random accepted / rejected decision:
                    new ExpressionBuilder().createGroovyScriptExpression("random decision", "new java.util.Random(System.currentTimeMillis()).nextBoolean()", "java.lang.Boolean")
            );
        } catch (InvalidExpressionException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        processBuilder.addDefaultTransition("gateway", "end"); // Default transition, taken is expression above returns false
        
		try {
			BusinessArchive businessArchive = new BusinessArchiveBuilder().createNewBusinessArchive()
					.setProcessDefinition(processBuilder.getProcess()).setActorMapping(actorMapping).done();

			ProcessDefinition deploy = null;
			long start1 = System.currentTimeMillis();
			
			try {
				deploy = apiClient.getProcessAPI().deploy(businessArchive);
			} catch (ProcessDeployException e) {
				e.printStackTrace();
			} 

			System.out.println(System.currentTimeMillis() - start1);

			apiClient.getProcessAPI().enableProcess(deploy.getId());

			HashMap<String, Serializable> hashMap = new HashMap<String, Serializable>();
			hashMap.put("var0", 12000.0);

			List<Operation> listOperations = new ArrayList<>();

			for (String variableName : hashMap.keySet()) {
				Operation operation = buildAssignOperation(variableName, hashMap.get(variableName).toString());
				listOperations.add(operation);
			}

			ProcessInstance startProcess = apiClient.getProcessAPI().startProcess(deploy.getId(), listOperations,
					hashMap);

			HumanTaskInstance lastStateHumanTaskInstance = null;

			long start = System.currentTimeMillis();
			for (int v = 0; v < vars; v++) {
				hashMap = new HashMap<String, Serializable>();
				hashMap.put("var" + v, 12000.0);

				while ((lastStateHumanTaskInstance = getHumanTaskInstance(startProcess.getRootProcessInstanceId(),
						"UserTask" + v)) == null)
					;

				long lastStateHumanTaskInstanceId = lastStateHumanTaskInstance.getId();

				while (apiClient.getProcessAPI().getActivityInstanceState(lastStateHumanTaskInstanceId)
						.equals("initializing"))
					;

				apiClient.getProcessAPI().assignAndExecuteUserTask(requester.getId(),
						lastStateHumanTaskInstanceId, hashMap);

				while (!canNextUserTask(lastStateHumanTaskInstanceId).equals("completed"))
					;
				// System.out.println("Depois " +
				// apiClient.getProcessAPI().getActivityInstanceState(lastStateHumanTaskInstance.getId()));
			}
			System.out.println("Runtime: " + (System.currentTimeMillis() - start));

			// Thread.sleep(1000);
			// System.out.println(apiClient.getProcessAPI().getOneAssignedUserTaskInstanceOfProcessInstance(startProcess.getRootProcessInstanceId(),
			// requester.getId()));
			// byte[] exportBarProcessContentUnderHome =
			// apiClient.getProcessAPI().exportBarProcessContentUnderHome(deploy.getId());
			// String nomeArquivo = "arquivo.bar"; // Nome do arquivo que você quer criar

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static HumanTaskInstance getHumanTaskInstance(long id, String task) {
		try {
			return apiClient.getProcessAPI()
					.getLastStateHumanTaskInstance(id, task);
		} catch (Exception e) {
			return null;
		}
	}

	private static String canNextUserTask(long lastStateHumanTaskInstance) {
		try {
			return apiClient.getProcessAPI().getActivityInstanceState(lastStateHumanTaskInstance);
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	private static Operation buildAssignOperation(String variable, String value) throws InvalidExpressionException {
		final Expression expression = new ExpressionBuilder().createGroovyScriptExpression(variable,
				"return 2 * " + value + "d", "java.lang.Double");
		final Operation operation = new OperationBuilder().createSetDataOperation(variable, expression);

		return operation;
	}

	@Bean
	public APIClient configureApiClient() {
		return new APIClient();
	}

	private static void loginAsTenantAdministrator() {
		try {
			apiClient.logout();
			apiClient.login(TENANT_ADMIN_NAME, TENANT_ADMIN_PASSWORD);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void loginWithAnotherUser(User newUser) {
		try {
			apiClient.logout();
			apiClient.login(newUser.getUserName(), "bpm");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static User createNewUser(String userName, String password, String firstName, String lastName) {
		try {
			return apiClient.getIdentityAPI().createUser(userName, password, firstName, lastName);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	private static User getUserByUserName(String userName) {
		try {
			return apiClient.getIdentityAPI().getUserByUserName(userName);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
