/**
 * Copyright (C) 2019 Bonitasoft S.A.
 * Bonitasoft, 32 rue Gustave Eiffel - 38000 Grenoble
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation
 * version 2.1 of the License.
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301, USA.
 **/
package org.bonitasoft.example.api;

import org.bonitasoft.engine.api.APIClient;
import org.bonitasoft.engine.bpm.flownode.HumanTaskInstance;
import org.bonitasoft.engine.identity.User;
import org.bonitasoft.engine.search.SearchOptionsBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

@RestController
public class TaskController {

    private APIClient apiClient;

    @Autowired
    public TaskController(APIClient apiClient) {
        this.apiClient = apiClient;
    }

    @GetMapping("/tasks")
    public List<HumanTaskInstance> list() {
        List<HumanTaskInstance> result = null;

        try {
            apiClient.login("requester", "bpm");
            result = apiClient.getProcessAPI().searchMyAvailableHumanTasks(apiClient.getSession().getId(),
                    new SearchOptionsBuilder(0, 100).done()).getResult();
            apiClient.logout();
        } catch (Exception e) {
        }
        return result;
    }

    @GetMapping("/task/{taskId}/executeAsValidator")
    public void executeFirstHumanTask(@PathVariable Long taskId) {
        try {
            apiClient.login("install", "install");
            User user = apiClient.getIdentityAPI().getUserByUserName("validator");
            apiClient.logout();
            apiClient.login("validator", "bpm");
            apiClient.getProcessAPI().assignAndExecuteUserTask(user.getId(), taskId, new HashMap<>());
            apiClient.logout();
        } catch (Exception e) {
        }
    }

    @GetMapping("/task/{taskId}/executeAsRequester")
    public void executeSignContractTask(@PathVariable Long taskId) {
        try {
            apiClient.login("install", "install");
            User user = apiClient.getIdentityAPI().getUserByUserName("requester");
            apiClient.logout();
            apiClient.login("requester", "bpm");
            apiClient.getProcessAPI().assignAndExecuteUserTask(user.getId(), taskId, new HashMap<>());
            apiClient.logout();
        } catch (Exception e) {
        }
    }

}