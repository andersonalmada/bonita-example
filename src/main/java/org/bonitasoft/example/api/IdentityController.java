/**
 * Copyright (C) 2019 Bonitasoft S.A.
 * Bonitasoft, 32 rue Gustave Eiffel - 38000 Grenoble
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation
 * version 2.1 of the License.
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301, USA.
 **/
package org.bonitasoft.example.api;

import org.bonitasoft.engine.api.APIClient;
import org.bonitasoft.engine.identity.User;
import org.bonitasoft.engine.search.SearchOptionsBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
public class IdentityController {

    private APIClient apiClient;

    @Autowired
    public IdentityController(APIClient apiClient) {
        this.apiClient = apiClient;
    }

    @GetMapping("/users")
    public List<User> list() {
        List<User> listAux = null;

        try {
            apiClient.login("install", "install");
            listAux = apiClient.getIdentityAPI().searchUsers(new SearchOptionsBuilder(0, 50).done()).getResult();
            apiClient.logout();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return listAux;
    }

}