/**
 * Copyright (C) 2019 Bonitasoft S.A.
 * Bonitasoft, 32 rue Gustave Eiffel - 38000 Grenoble
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation
 * version 2.1 of the License.
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301, USA.
 **/
package org.bonitasoft.example.api;

import org.bonitasoft.engine.api.APIClient;
import org.bonitasoft.engine.bpm.process.ArchivedProcessInstance;
import org.bonitasoft.engine.bpm.process.ProcessInstance;
import org.bonitasoft.engine.search.SearchOptionsBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
public class CaseController {

    private APIClient apiClient;

    @Autowired
    public CaseController(APIClient apiClient) {
        this.apiClient = apiClient;
    }

    // Expose the open process instances (=cases not completed)
    @GetMapping("/cases")
    public List<ProcessInstance> list()  {
        List<ProcessInstance> listAux = null;

        try {
            apiClient.login("install", "install");
            listAux = apiClient.getProcessAPI().searchOpenProcessInstances(new SearchOptionsBuilder(0, 100).done()).getResult();
            apiClient.logout();
        } catch(Exception e) {
            e.printStackTrace();
        } 

        return listAux;
    }

    // Expose the finished process instances (=cases completed)
    @GetMapping("/completedcases")
    public List<ArchivedProcessInstance> listCompleted()  {
        List<ArchivedProcessInstance> listAux = null;

        try {
            apiClient.login("install", "install");
            listAux = apiClient.getProcessAPI().searchArchivedProcessInstances(new SearchOptionsBuilder(0, 100).done()).getResult();
            apiClient.logout();
        } catch(Exception e) {
            e.printStackTrace();
        } 

        return listAux;
    }

}