/**
 * Copyright (C) 2019 Bonitasoft S.A.
 * Bonitasoft, 32 rue Gustave Eiffel - 38000 Grenoble
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation
 * version 2.1 of the License.
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301, USA.
 **/
package org.bonitasoft.example.api;

import org.bonitasoft.engine.api.APIClient;
import org.bonitasoft.engine.bpm.process.ProcessDeploymentInfo;
import org.bonitasoft.engine.search.SearchOptionsBuilder;
import static org.bonitasoft.example.process.LoanRequestProcessBuilder.CONTRACT_AMOUNT;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

@RestController
public class ProcessController {

    private APIClient apiClient;

    @Autowired
    public ProcessController(APIClient apiClient) {
        this.apiClient = apiClient;
    }

    // Expose the deployed processes through Rest Apis:
    @GetMapping("/processes")
    public List<ProcessDeploymentInfo> list() {
        List<ProcessDeploymentInfo> result = null;

        try {
            apiClient.login("install", "install");
            result = apiClient.getProcessAPI().searchProcessDeploymentInfos(new SearchOptionsBuilder(0, 100).done())
                    .getResult();
            result.forEach(it -> {
                System.out.println(it.getProcessId());
            });

            apiClient.logout();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @GetMapping("/process/{id}/undeploy")
    public void uninstall(@PathVariable Long id) {
        try {
            apiClient.login("install", "install");
            apiClient.getProcessAPI().disableAndDeleteProcessDefinition(id);
            apiClient.logout();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @GetMapping("/process/{id}/start")
    public void startProcess(@PathVariable Long id) {
        try {
            apiClient.login("install", "install");
            HashMap<String, Serializable> hashMap = new HashMap<>();
            hashMap.put(CONTRACT_AMOUNT, 12000.0);

            apiClient.getProcessAPI().startProcess(id, hashMap);
            apiClient.logout();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}