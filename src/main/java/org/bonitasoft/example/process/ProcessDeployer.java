package org.bonitasoft.example.process;

import org.bonitasoft.engine.bpm.bar.BusinessArchive;
import org.bonitasoft.engine.bpm.bar.BusinessArchiveBuilder;
import org.bonitasoft.engine.bpm.bar.actorMapping.Actor;
import org.bonitasoft.engine.bpm.bar.actorMapping.ActorMapping;
import org.bonitasoft.engine.bpm.process.DesignProcessDefinition;
import org.bonitasoft.engine.bpm.process.ProcessDefinition;
import org.bonitasoft.engine.identity.User;
import org.bonitasoft.engine.api.APIClient;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Emmanuel Duchastenier
 */
public class ProcessDeployer {

    private APIClient apiClient;

    @Autowired
    public ProcessDeployer(APIClient apiClient) {
        this.apiClient = apiClient;
    }

    public ProcessDefinition deployAndEnableProcessWithActor(DesignProcessDefinition designProcessDefinition,
            String requesterActor,
            User requesterUser,
            String validatorActor,
            User validatorUser) {
        try {
            // Create the Actor Mapping with our Users:
            Actor requester = new Actor(requesterActor);
            requester.addUser(requesterUser.getUserName());
            Actor validator = new Actor(validatorActor);
            validator.addUser(validatorUser.getUserName());
            ActorMapping actorMapping = new ActorMapping();
            actorMapping.addActor(requester);
            actorMapping.addActor(validator);

            // Create the Business Archive to deploy
            BusinessArchive businessArchive = new BusinessArchiveBuilder().createNewBusinessArchive()
                    .setProcessDefinition(designProcessDefinition)
                    // set the actor mapping so that the process is resolved and can then be
                    // enabled:
                    .setActorMapping(actorMapping).done();

            ProcessDefinition deploy = apiClient.getProcessAPI().deploy(businessArchive);
            apiClient.getProcessAPI().enableProcess(deploy.getId());
            return deploy;
        } catch (Exception e) {

        }

        return null;
    }

    public ProcessDefinition deployAndEnableBusinessArchiveWithoutAnyActor(BusinessArchive businessArchive) {
        try {
            ProcessDefinition deploy = apiClient.getProcessAPI().deploy(businessArchive);
            apiClient.getProcessAPI().enableProcess(deploy.getId());
            return deploy;
        } catch (Exception e) {

        }

        return null;
    }
}
